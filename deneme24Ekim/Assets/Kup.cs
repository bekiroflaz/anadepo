﻿using UnityEngine;
using System.Collections;

public class Kup : MonoBehaviour {
    public int hiz;
    // Use this for initialization
    void Start () {
        if (gameObject.name == "maviKup(Clone)")
            hiz = 1;
        else if (gameObject.name == "yesilKup(Clone)")
            hiz = 2;
        else
            hiz = 3;

	}
	// Update is called once per frame
	void Update ()
    {
        gameObject.transform.position = gameObject.transform.position + new Vector3(Time.deltaTime*hiz, 0, 0);
	}
}

public enum Renk { Yesil, Mavi, Kirmizi}
