﻿using UnityEngine;
using System.Collections;

public class script : MonoBehaviour {
    public GameObject maviKup;
    public GameObject yesilKup;
    public GameObject kirmiziKup;
    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
            Instantiate(maviKup, new Vector3(0, 0, 0), Quaternion.identity);

        if (Input.GetKeyDown(KeyCode.Backspace))
            Instantiate(yesilKup, new Vector3(1, 0, 0), Quaternion.identity);

        if (Input.GetKeyDown(KeyCode.A))
            Instantiate(kirmiziKup, new Vector3(2, 0, 0), Quaternion.identity);
    }
}
